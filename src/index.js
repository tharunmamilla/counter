import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
import "bootstrap/dist/css/bootstrap.css";
import * as serviceWorker from './serviceWorker';
//import Nameform from "./components/nameform";
//import Header from "./components/header";

import NameForm from './components/nameform';
ReactDOM.render(<NameForm />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
