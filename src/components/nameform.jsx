import React, { Component } from 'react';

class NameForm extends Component {

  constructor(props) {
    super(props)
    this.textInput = React.createRef();
    this.state = { fullname: '' }
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit = (event) => {

    //alert(`${this.state.fullname}`)
    event.preventDefault()
    this.setState({ fullname: this.textInput.current.value })
  }
  // handleInputChange = (event) => {
  //   event.preventDefault()

  //   this.setState({
  //     fullname: event.target.value
  //   })


  // };
  render() {
    const { fullname } = this.state
    return (

      <div>
        <h1>forms</h1>
        <p>

          <header style={{ backgroundColor: "black", fontSize: 100, width: "100%", color: "white", marginRight: 100, textAlign: "center" }} >
            <a>{this.state.fullname}</a>

          </header>
        </p>
        <form onSubmit={this.handleSubmit}>

          <input type="text" placeholder="enter name" ref={this.textInput} />
          <footer style={{ backgroundColor: "black", fontSize: 100, width: "100%", color: "white", position: "absolute", bottom: 0, textAlign: "center" }}>
            <a>{this.state.fullname} </a>
          </footer>

          <p><button onClick={this.handlesubmit} >submit</button></p>
        </form>
      </div>
    )
  }
}
export default NameForm;