import React, { Component } from 'react';
class Counter extends Component {
    constructor(props) {
        super(props);
        this.state = { value: '' };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({ value: this.element.value });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <header style={{ backgroundColor: "black", fontSize: 100, width: "100%", color: "white", marginRight: 100, textAlign: "center" }} >
                        <a>{this.state.value}</a>

                    </header>
                    <label>
                        <input type="text" /> />
                    </label>

                    <input type="submit" value="Submit" />
                </form>
                {/* <p>{this.state.value}</p> */}
            </div>
        );
    }
}
export default Counter;